import axios from "axios";

const instance = axios.create({
  baseURL: "http://lacalhost:3000",
});
export default instance;
